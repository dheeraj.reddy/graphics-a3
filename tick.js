var t=0;
function tick() {
    if(!hero.tick()){
        ctx.fillText(`GAME OVER`,90,50);
        ctx.fillText(`Score:${hero.score}`,110,70);
        return false;
    }
	if(t>=30){
	wall2.material.map=texturewall;wall1.material.map=texturewall;
	}
	else{
		wall2.material.map=texturewallg;wall1.material.map=texturewallg;
	}
	t++;
	t%=60;

    //coins
c.forEach(it => {
    it.obj.rotation.y+=0.1;
    if((it.obj.position.x-hero.obj.position.x+1) *(it.obj.position.x-hero.obj.position.x-1)<0)
        if((it.obj.position.y-hero.obj.position.y+1) *(it.obj.position.y-hero.obj.position.y-1)<0)
             if((it.obj.position.z-hero.obj.position.z+1) *(it.obj.position.z-hero.obj.position.z-1)<0)
                 { it.isdead=true;hero.score+=10;sound.play();}

    if(it.obj.position.z > hero.obj.position.z+5)
        it.isdead=true;
    // scene.remove(it.obj);   
});
for (var i = c.length -1; i >= 0; i--)
if(c[i].isdead)
   {
       scene.remove(c[i].obj);
       c.splice(i,1);
   }

//poles


p.forEach(it => {
 
    if((it.obj.position.x-hero.obj.position.x+1) *(it.obj.position.x-hero.obj.position.x-1)<0)
        if((it.obj.position.y-hero.obj.position.y+1) *(it.obj.position.y-hero.obj.position.y+5)<0)
             if((it.obj.position.z-hero.obj.position.z+0.4) *(it.obj.position.z-hero.obj.position.z-0.4)<=0)
                { it.isdead=true;hero.confuse();}
              

    if(it.obj.position.z > hero.obj.position.z+5)
        it.isdead=true;
    // scene.remove(it.obj);   
});
for (var i = p.length -1; i >= 0; i--)
if(p[i].isdead)
   {
       scene.remove(p[i].obj);
       p.splice(i,1);
   }



//rocks

r.forEach(it => {
    it.obj.rotation.y+=0.1;
    if((it.obj.position.x-hero.obj.position.x+1) *(it.obj.position.x-hero.obj.position.x-1)<0)
        if((it.obj.position.y-hero.obj.position.y+1) *(it.obj.position.y-hero.obj.position.y-1)<0)
             if((it.obj.position.z-hero.obj.position.z+1) *(it.obj.position.z-hero.obj.position.z-1)<0)
                 { it.isdead=true;hero.confuse();}

    if(it.obj.position.z > hero.obj.position.z+5)
        it.isdead=true;
    // scene.remove(it.obj);   
});
for (var i = r.length -1; i >= 0; i--)
if(r[i].isdead)
   {
       scene.remove(r[i].obj);
       r.splice(i,1);
   }





//police
police.tick();
//dog
dog.tick();
// jump1
j1.forEach(it => {
    
    if((it.obj.position.x-hero.obj.position.x+1) *(it.obj.position.x-hero.obj.position.x-1)<0)
        if((it.obj.position.y-hero.obj.position.y+0.5) *(it.obj.position.y-hero.obj.position.y+2.5)<0)
             if((it.obj.position.z-hero.obj.position.z+1) *(it.obj.position.z-hero.obj.position.z-1)<0)
                 { it.isdead=true;hero.isdead=true;}

    if(it.obj.position.z > hero.obj.position.z+5)
        it.isdead=true;
    // scene.remove(it.obj);   
});
for (var i = j1.length -1; i >= 0; i--)
if(j1[i].isdead)
   {
       scene.remove(j1[i].obj);
       j1.splice(i,1);
   }





// slide1


s1.forEach(it => {
    
    if((it.obj.position.x-hero.obj.position.x+1) *(it.obj.position.x-hero.obj.position.x-1)<0)
        if((it.obj.position.y-hero.obj.position.y) *(it.obj.position.y-hero.obj.position.y+2)<0)
             if((it.obj.position.z-hero.obj.position.z) *(it.obj.position.z-hero.obj.position.z-0.5)<0)
                if(hero.obj.rotation.x==0)
                 { it.isdead=true;hero.isdead=true;}

    if(it.obj.position.z > hero.obj.position.z+5)
        it.isdead=true;
    // scene.remove(it.obj);   
});
for (var i = s1.length -1; i >= 0; i--)
if(s1[i].isdead)
   {
       scene.remove(s1[i].obj);
       s1.splice(i,1);
   }


// bon1
b1.forEach(it => {
    
    if((it.obj.position.x-hero.obj.position.x+1) *(it.obj.position.x-hero.obj.position.x-1)<0)
        if((it.obj.position.y-hero.obj.position.y+1) *(it.obj.position.y-hero.obj.position.y-1)<0)
             if((it.obj.position.z-hero.obj.position.z+1) *(it.obj.position.z-hero.obj.position.z-1)<0)
                 { it.isdead=true;hero.score+=20;hero.flyh();}

    if(it.obj.position.z > hero.obj.position.z+5)
        it.isdead=true;
    // scene.remove(it.obj);   
});
for (var i = b1.length -1; i >= 0; i--)
if(b1[i].isdead)
   {
       scene.remove(b1[i].obj);
       b1.splice(i,1);
   }
// bon2
b2.forEach(it => {
    it.obj.rotation.y+=0.1;
    if((it.obj.position.x-hero.obj.position.x+1) *(it.obj.position.x-hero.obj.position.x-1)<0)
        if((it.obj.position.y-hero.obj.position.y+1) *(it.obj.position.y-hero.obj.position.y+3)<0)
             if((it.obj.position.z-hero.obj.position.z+1) *(it.obj.position.z-hero.obj.position.z-1)<0)
                 { it.isdead=true;hero.score+=20;hero.highj();}

    if(it.obj.position.z > hero.obj.position.z+5)
        it.isdead=true;
    // scene.remove(it.obj);   
});
for (var i = b2.length -1; i >= 0; i--)
if(b2[i].isdead)
   {
       scene.remove(b2[i].obj);
       b2.splice(i,1);
   }
//train
// console.log(tra[0].obj.position,hero.obj.position);
hero.ontrain=false;
tra.forEach(it => {
    
    if((it.obj.position.x-hero.obj.position.x+1) *(it.obj.position.x-hero.obj.position.x-1)<0)
        if((it.obj.position.y-hero.obj.position.y+7) *(it.obj.position.y-hero.obj.position.y)<=0)
             if((it.obj.position.z-hero.obj.position.z+15+1) *(it.obj.position.z-hero.obj.position.z+15-1)<0)
                 { hero.isdead=true;}
    
    if((it.obj.position.x-hero.obj.position.x+1) *(it.obj.position.x-hero.obj.position.x-1)<0)
    if((it.obj.position.y-hero.obj.position.y+7.5) *(it.obj.position.y-hero.obj.position.y+8.3)<=0)
        if((it.obj.position.z-hero.obj.position.z+32) *(it.obj.position.z-hero.obj.position.z-32)<0)
            { hero.ontrain=true;}

    if(it.obj.position.z > hero.obj.position.z+18)
        it.isdead=true;
    // scene.remove(it.obj);   
});
for (var i = tra.length -1; i >= 0; i--)
if(tra[i].isdead)
   {
       scene.remove(tra[i].obj);
       tra.splice(i,1);
   }




camera.position.set(pp.x,pp.y,pp.z);

}