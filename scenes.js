var scene = new THREE.Scene();
scene.background = new THREE.Color( "rgb(76, 203, 255)" );
scene.fog=new THREE.Fog(new THREE.Color("rgb(47,79,79)"),2,200);

var camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight,
  0.1,
  1000
);
var ll=new THREE.Vector3(0,0,0)
camera.lookAt(ll);
var pp=new THREE.Vector3(0,0,0)
var light = new THREE.AmbientLight(0xbbbbbb); // soft white light
scene.add(light);

var listener = new THREE.AudioListener();
camera.add( listener );
var sound = new THREE.Audio( listener );

// load a sound and set it as the Audio object's buffer
var audioLoader = new THREE.AudioLoader();
audioLoader.load( 'coin.mp3', function( buffer ) {
	sound.setBuffer( buffer );
	sound.setLoop( false );
	sound.setVolume( 0.5 );
});
var gg=true;

var renderer = new THREE.WebGLRenderer({canvas:document.querySelector('#subway')});
renderer.setSize(window.innerWidth, window.innerHeight);
// document.body.appendChild(renderer.domElement);
// postprocessing

  var composer = new THREE.EffectComposer( renderer );
  var renderPass = new THREE.RenderPass( scene, camera );
  composer.addPass( renderPass );

  // color to grayscale conversion

  var effectGrayScale = new THREE.ShaderPass( THREE.LuminosityShader );
  effectGrayScale.renderToScreen = true;
  composer.addPass( effectGrayScale );
camera.position.z = 15;
camera.position.y=10;
var cl=new THREE.Clock();
var cl1=new THREE.Clock();
var cl2=new THREE.Clock();
var scorecanvas=document.querySelector('#score');
var ctx=scorecanvas.getContext("2d");
ctx.font= "20px Arial";